# Just One Giant Lab Stack

Welcome on our repository! Below you will find information on how to help, as well as how to run the code.

# How to contribute

JOGL is **100% open source**, and we fully welcome contributions! You can help us by **contributing to the code**, or translating the platform in your language.

We use the following technologies: [ReactJS](https://reactjs.org/), [NextJS](https://github.com/vercel/next.js/), Typescript, Algolia, Sass, AmazonS3, Ruby, GraphQL, Elastic Search... If you have experience in one or multiple of them, your help will be much appreciated!

Feel free to browse through the issues and see if you can help us in one or multiple of them:

- **List view** - View issues in list ([link here](https://gitlab.com/JOGL/JOGL/-/issues)).
- **Board view** - View a Trello-like view of the issues listed by priority and state: to do, doing, ready/review ([link here](https://gitlab.com/JOGL/JOGL/-/boards/885598)).

You can also contact us at support[at]jogl.io for any question.

For more detailed information on how to contribute, please read our [Contributing guidelines](https://gitlab.com/JOGL/JOGL/-/blob/master/CONTRIBUTING.md).

# How to report a bug or add add a feature request.

Please have a quick check through our [open issues](https://gitlab.com/JOGL/JOGL/-/issues) if your issue/problem/request has already been reported.

If so, you can click on :thumbsup: to indicate that you have the same issue, and you can add a comment to the issue to give more detail. If not, go ahead and **create your new issue** using [this link](https://gitlab.com/JOGL/JOGL/-/issues/new).

# How to use JOGL

## Introduction

The stack is composed of two apps, a Frontend and a Backend. They both have their own repositories located here:

- Backend: https://gitlab.com/JOGL/backend-v0.1
- Frontend: https://gitlab.com/JOGL/frontend-v0.1

We use Docker to manage all the stack and its dependencies, this allows us to create a self contained group of apps running easily, and orchestrable using docker-compose, or kubernetes.

Each of the three repositories contains a `docker-compose.yaml` file which orchestrates different things:

- JOGL: This docker-compose creates an entire self contained stack with databases and other services.
- frontend-v0.1: This docker-compose is used to build a production ready container for the frontend.
- backend-v0.1: This docker-compose is used to build a production ready container for the backend and sidekiq.

## Dependencies

JOGL relies on open technologies for almost everything. Sadly due to our limited team, we are using Algolia as a search service on our website, but we are planning in the short term to move to Elasticsearch. In the mean time, you must open a free Algolia account to use this stack.

## Clone the repository

First clone the repository `git clone https://gitlab.com/JOGL/JOGL.git` or `git clone git@gitlab.com:JOGL/JOGL.git` if you have a your ssh git setup (**Recommended for ease of use and ease of PR and MR**).

You will need to init the submodules with `git submodule update --init --recursive`

## Update the latest code from both code stack

This will update both the frontend and backend which are git submodules. It will pull from the master branch FYI

`git submodule update --recursive --remote`

## Configuration

### Local environment

You will need to create a `.env.local` file containing the following variables (You can copy `.env.example` to get there faster!)

```
# Define the URL at which your two services will anwser
FRONTEND_URL=http://localhost:3000
BACKEND_URL=http://localhost:3001
# If working locally, you want to set RAILS_ENV to development, otherwise in production you want to set it to production
RAILS_ENV=development
# Algolia part
ALGOLIA_API_TOKEN=[Your Aloglia API Token]
ALGOLIA_APP_ID=[Your Algolia APP ID Token]
# Optional variables
# If you want to use Amazon S3 services for file caching, that is where you put your code, otherwise the app will default to local storage (See ActiveStorage for more documentation)
AWS_S3_KEY_ID=[Your Amazon S3 key ID]
AWS_S3_SECRET=[Your Amazon S3 Secret]
# If you want to forward your App Logs to a ELK stack for analysis you can use those two variables, we use the logstasher gem.
LOGSTASH_SERVER=[Your Logstash server URL]
LOGSTASH_PORT=[Your Logstash server Port]
```

### Production environment

On top of the ENV variable defined above, you will also need to setup the following variables

```
# Hosts
FRONTEND_URL=[Your frontend URL]
BACKEND_URL=[Your backend URL]
# Rails server
RAILS_ENV=production
# Databases
DATABASE_URL=[Postgres URL format: postgres://user:psw@host:port/db_name]
REDIS_URL=[REDIS URL format: redis://user:psw@host:port]
SECRET_KEY_BASE=[A long random number]
JOGL_EMAIL=[The email that you want the notifications to be]
# Optional if you want your users to register to you mailing list on Mailchimp
MAILCHIMP_API_KEY=[Your Mailchimp API key]
MAILCHIMP_LIST_ID=[Your Mailchimp list ID]
WEB_CONCURRENCY=[The number of parallel puma workers]

# SMTP server settings
SMTP_URL
SMTP_PORT
SMTP_DOMAIN
SMTP_USER
SMTP_PASSWORD
```

## Build the docker images

Next we need to build the docker images for the stack, this needs to be repeated everytime you make a change in the code and want to test it in the docker environment.

`docker-compose build`

This will create 3 images `frontend` `backend` `sidekiq` which we will then start.

## Backend DB migration and seed

In order for the backend to work we need to migrate the database. It's probably a good idea to do this everytime you pull some new code from the backend (at least the migrate part). Also we need to make sure the interests are seeded in the database!

- `docker-compose run backend bundle exec rails db:create db:migrate db:seed`

## Start the docker stack

docker-compose will run the images we just created along with the necessary databases and maildev.

`docker-compose up -d`

## Working inside this repository

You do not need to pull the frontend or backend repository, you can work directly from the submodules. To do so:

- Navigate to the frontend or backend repo `cd JOGL-Frontend` or `cd JOGL-Backend`
- Checkout the develop branch `git checkout develop`
- Pull the last version `git pull`
- Create a new branch `git branch myFeature`
- Once finished commit your code `git commit -a`
- Merge it back to develop `git checkout develop` followed by `git merge myFeature`
- Create a merge request

## Frontend only development

Refer to the [Frontend git repo](https://gitlab.com/JOGL/frontend-v0.1) for Readme onto how to get the code working locally.

## Backend only development

Refer to the [Backend git repo](https://gitlab.com/JOGL/backend-v0.1) for Readme onto how to get the code working locally. But you will need to have DBs and all started, we recommend working from the JOGL repository.

# Tips and tricks

If you need to rollback a change you made to the DB:schema you can use `docker-compose run backend bundle exec rails db:rollback`

## Running Rspec tests on the Backend

After you create something new and you want to run the test locally, you will need to launch rspec. You can do this from your terminal with

`docker-compose run -e RAILS_ENV=test backend bundle exec rspec`

\*Note the `-e RAILS_ENV=test` that is critical! Otherwise rails does not know it is in test mode.

## backend and frontend package update

If you want to add a npm package, or a gem, then you need to install them in the docker container. To do that, just rebuild the docker image AFTER you changed the package.json, or the Gemfile.

- backend: `docker-compose build backend sidekiq`
- frontend: `docker-compose build frontend`

Then just recreate instances with:

- backend: `docker-compose up -d backend sidekiq`
- frontend: `docker-compose up -d frontend`

## What's where

You should find the different elements on those ports:

- backend: `http://localhost:3001`
- frontend: `http://localhost:3000`
